Name:     bash
Version:  5.2.37
Release:  2
Summary:  It is the Bourne Again Shell
License:  GPL-3.0-or-later
URL:      https://www.gnu.org/software/bash
Source0:  https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz

Source1: dot-bashrc
Source2: dot-bash_profile
Source3: dot-bash_logout

# PATCH-FIX-UPSTREAM
Patch115: bash-2.05a-interpreter.patch
Patch118: bash-2.05b-pgrp_sync.patch
Patch125: bash-4.0-nobits.patch
Patch129: bash-4.2-coverity.patch
# rh1102815
Patch133: bash-4.3-noecho.patch
# fix bash leaks memory when LC_ALL set-rh1241533,rh1224855
Patch134: bash-4.3-memleak-lc_all.patch
Patch137: bugfix-Forbidden-non-root-user-to-clear-history.patch
Patch138: enable-dot-logout-and-source-bashrc-through-ssh.patch
Patch139: cd-alias.patch
Patch140: bash-5.1-sw.patch

Patch6000: backport-fix-for-nofork-comsub-command-printing-fix-for-crash.patch
Patch6001: backport-fix-small-memleak-in-globbing.patch
Patch6002: backport-fix-for-leak-when-completing-command-word-with-glob-.patch
Patch6003: backport-fix-for-E-transformation-fixes-for-failures-in-arith.patch
Patch6004: backport-changes-for-quoting-special-and-multibyte-characters.patch
Patch6005: backport-fixes-for-LINENO-in-multi-line-simple-commands-print.patch
Patch6006: backport-changes-to-SIGINT-handler-while-waiting-for-a-child-.patch
Patch6007: backport-renamed-several-functions-beginning-with-legal_-chan.patch
Patch6008: backport-fix-for-cd-when-curent-directory-doesn-t-exist-fix-w.patch

BuildRequires: gcc bison texinfo autoconf ncurses-devel
# Required for bash tests
BuildRequires: glibc-all-langpacks

Requires:      filesystem

Provides:      /bin/sh /bin/bash

%description
Bash is the GNU Project's shell. Bash is the Bourne Again SHell. Bash is an sh-compatible
shell that incorporates useful features from the Korn shell (ksh) and C shell (csh). It is
intended to conform to the IEEE POSIX P1003.2/ISO 9945.2 Shell and Tools standard. It offers
functional improvements over sh for both programming and interactive use. In addition, most
sh scripts can be run by Bash without modification.

%package devel
Summary: Development headers for %{name}
Requires: %{name} = %{version}-%{release}
Requires: pkgconf-pkg-config

%description devel
This package contains development files for %{name}.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:	man info
Provides:       %{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release}

%description help
Man pages and other related documents for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoconf
%configure --with-bash-malloc=no --with-afs
MFLAGS="CPPFLAGS=-D_GNU_SOURCE -DRECYCLES_PIDS -DDEFAULT_PATH_VALUE='\"/usr/local/bin:/usr/bin\"' `getconf LFS_CFLAGS`"
make "$MFLAGS" version.h
make "$MFLAGS" -C builtins
%make_build "$MFLAGS"

%install
%make_install install-headers

# make manpages for bash builtins as per suggestion in DOC/README
pushd doc
sed -e '
/^\.SH NAME/, /\\- bash built-in commands, see \\fBbash\\fR(1)$/{
/^\.SH NAME/d
s/^bash, //
s/\\- bash built-in commands, see \\fBbash\\fR(1)$//
s/,//g
b
}
d
' builtins.1 > man.pages
for i in echo pwd test kill; do
  sed -i -e "s,$i,,g" man.pages
  sed -i -e "s,  , ,g" man.pages
done

install -p -m 644 builtins.1 %{buildroot}%{_mandir}/man1/builtins.1

for i in `cat man.pages` ; do
  echo .so man1/builtins.1 > %{buildroot}%{_mandir}/man1/$i.1
  chmod 0644 %{buildroot}%{_mandir}/man1/$i.1
done
popd

# Link bash man page to sh so that man sh works.
ln -s bash.1 %{buildroot}%{_mandir}/man1/sh.1

# Not for printf, true and false (conflict with coreutils)
rm -f %{buildroot}/%{_mandir}/man1/printf.1
rm -f %{buildroot}/%{_mandir}/man1/true.1
rm -f %{buildroot}/%{_mandir}/man1/false.1

ln -sf bash %{buildroot}%{_bindir}/sh
install -pDm 644 %SOURCE1 %{buildroot}/etc/skel/.bashrc
install -pDm 644 %SOURCE2 %{buildroot}/etc/skel/.bash_profile
install -pDm 644 %SOURCE3 %{buildroot}/etc/skel/.bash_logout
install -pDm 644 ./configs/alias.sh %{buildroot}%{_sysconfdir}/profile.d/alias.sh

# bug #820192, need to add execable alternatives for regular built-ins
for ea in alias bg cd command fc fg getopts hash jobs read type ulimit umask unalias wait
do
  cat <<EOF > "%{buildroot}"/%{_bindir}/"$ea"
#!/bin/sh
builtin $ea "\$@"
EOF
chmod +x "%{buildroot}"/%{_bindir}/"$ea"
done

%find_lang %{name}

%check
make check

%files -f %{name}.lang
%defattr(-,root,root)
%license COPYING
%config(noreplace) /etc/skel/.b*
%{_bindir}/{sh,bash,alias,bg,cd,command,fc,fg,wait,bashbug}
%{_bindir}/{hash,getopts,jobs,read,type,ulimit,umask,unalias}
%config(noreplace) %{_sysconfdir}/profile.d/alias.sh

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/*.pc
%{_libdir}/bash/*
%exclude %{_libdir}/bash/Makefile*

%files help
%defattr(-,root,root)
%doc NOTES
%doc doc/*.ps doc/*.0 doc/*.html doc/*.txt
%doc %{_docdir}/%{name}/*
%{_infodir}/%{name}.info*
%{_mandir}/man1/*.gz
%{_mandir}/*/..1*
%exclude %{_infodir}/dir

%changelog
* Fri Nov 22 2024 wangyuhang <wangyuhang27@huawei.com> - 5.2.37-2
- del doesn't exist PATH dirs in bashrc

* Tue Sep 24 2024 Funda Wang <fundawang@yeah.net> - 5.2.37-1
- update to 5.2.37

* Wed Jul  3 2024 dillon chen <dillon.chen@gmail.com> - 5.2.21-1
- update to 5.2.21

* Wed May 08 2024 xiaozai <xiaozai@kylinos.cn> -5.2.15-9
- fix for a crash if one of the expressions in an arithmetic for command expands to NULL 

* Fri Apr 26 2024 wangyuhang <wangyuhang27@huawei.com> -5.2.15-8
- sync patches from bash community

* Mon Apr 22 2024 wangyuhang <wangyuhang27@huawei.com> -5.2.15-7
- relocations are no longer required.

* Wed Apr 10 2024 yueyuankun <yueyuankun@kylinos.cn> -5.2.15-6
- add manpages

* Mon Dec 4 2023 hongjinghao <hongjinghao@huawei.com> -5.2.15-5
- sync patches from bash community

* Tue Aug 29 2023 liutie <liutie4@huawei.com> -5.2.15-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove path /usr/bin for sub package relocation and fix issue

* Tue Aug 8 2023 longwei <longwei27@huawei.com> -5.2.15-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revert to use pie links and change position of bash.relocation

* Fri Aug 4 2023 longwei <longwei27@huawei.com> -5.2.15-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Add relocation package

* Mon Jul 17 2023 wangyuhang <wangyuhang27@huawei.com> -5.2.15-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: update to 5.2.15

* Thu Jun 15 2023 wangyuhang <wangyuhang27@huawei.com> -5.1.8-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: backport-Bash-5.1-patch-13-fix-tilde-expansion-after-unquoted.patch
        backport-Bash-5.1-patch-14-fix-off-by-one-error-when-reading-.patch
        backport-changes-to-documentation-to-reduce-gender-specific-l.patch
        backport-fixes-for-array-subscripts-and-values-containing-0x0.patch
        change PATCHLEVEL from 9 to 8, keep it consistent with the actual version of bash

* Mon Jan 16 2023 wangyuhang <wangyuhang27@huawei.com> -5.1.8-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add backport-Bash-5.1-patch-10-fix-for-wait-n-being-interrupted-b.patch
    backport-Bash-5.1-patch-11-save-and-restore-alias-parsing-whe.patch
    backport-Bash-5.1-patch-12-fix-race-condition-with-child-proc.patch
    backport-Bash-5.1-patch-15-fix-readline-display-of-some-chara.patch
    backport-Bash-5.1-patch-16-fix-interpretation-of-multiple-ins.patch

* Thu Nov 10 2022 wangyuhang <wangyuhang27@huawei.com> -5.1.8-6
- Type:CVE
- ID:NA
- SUG:NA
- DESC: fix CVE-2022-3715

* Sat Oct 29 2022 licihua <licihua@huawei.com> -5.1.8-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: fix crash in readline when started with an invalid locale specification

* Mon Aug 15 2022 panxiaohe <panxh.life@foxmail.com> - 5.1.8-4
- fix bugfix-Forbidden-non-root-user-to-clear-history.patch

* Wed Jul 20 2022 wuzx<wuzx1226@qq.com> - 5.1.8-3
- add sw64 patch

* Wed Jul 6 2022 zoulin <zoulin13@h-partners.com> - 5.1.8-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add alias.sh

* Thu Dec 30 2021 wangjie <wangjie375@huawei.com> - 5.1.8-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: Update bash to 5.1.8

* Fri Jun 18 2021 liujian<liujianliu.liu@huawei.com> - 5.1-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add upstream patches

* Mon May 31 2021 liujian<liujianliu.liu@huawei.com> - 5.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to 5.1

* Tue Mar 30 2021 shenyangyang<shenyangyang4@huawei.com> - 5.0-17
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Rebuild for openEuler-rpm-config moving /usr/lib/rpm/openEuler/xxxx
       to /usr/lib/xxxx

* Wed Jan 6 2021 Liquor <lirui130@huawei.com> - 5.0-16
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify file /etc/skel/.b* permissions to 644

* Sat Oct 31 2020 Liquor <lirui130@huawei.com> - 5.0-15
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix format error

* Thu Apr 30 2020 licihua <licihua@huawei.com> - 5.0-14
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add upstream patch

* Wed Apr 8 2020 openEuler Buildteam <licihua@huawei.com> - 5.0-13
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add upstream patches

* Thu Mar 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.0-12
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove comment in dot-bash_profile

* Tue Mar 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.0-11
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add build requires of ncurses-devel

* Fri Feb 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.0-10
- Type:CVE
- ID:NA
- SUG:NA
- DESC:CVE-2019-18276

* Wed Jan 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.0-9
- resolve compile problems.

* Mon Jan 6 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.0-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete redundant files

* Wed Dec 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.0-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add provides of bash-doc

* Thu Oct 24 2019 shenyangyang<shenyangyang4@huawei.com> - 5.0-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add build requires of bison and texinfo

* Fri Oct 11 2019 shenyangyang<shenyangyang4@huawei.com> - 5.0-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:move the man files

* Mon Sep 30 2019 shenyangyang<shenyangyang4@huawei.com> - 5.0-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:modify requires

* Sat Sep 21 2019 shenyangyang<shenyangyang4@huawei.com> - 5.0-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise description

* Wed Sep 4 2019 shenyangyang<shenyangyang4@huawei.com> - 5.0-2
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:remove man to main package

* Mon Sep 2 2019 shenyangyang<shenyangyang4@huawei.com> - 5.0-1
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:strenthen spec

* Sat Mar 16 2019 hanzhijun<hanzhijun1@huawei.com> - 4.4.23-7
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:fix pathname expansion of directory

* Fri Jan 25 2019 Xiaoqi Guo<guoxiaoqi2@huawei.com> - 4.4.23-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:1000 Forbidden non root user to clear history

* Mon Oct 08 2018 licunlong <licunlong@huawei.com> - 4.4.23-5
- Package Initialization
